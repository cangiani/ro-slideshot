Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  # devise_for :users

  get "lectures", controller: 'talks', action: 'index', category: 'lecture'

  # resources :slides
  resources :events, :only => [:index, :show] do
    get "embed", :on => :member
  end
  resources :talks, :only => [:index, :show]
  get "play/:id" => 'slideshots#show', :as=>:play_slideshot
  get "eplay/:id" => 'slideshots#embed', :as=>:play_embedded_slideshot

  root "events#index"
end
