Paperclip::UrlGenerator.class_eval do
  def escape_url(url)
    (url.respond_to?(:escape) ? url.escape : CGI.escape(url)).gsub(/(\/.+)\?(.+\.)/, '\1%3F\2')
  end
end
