# Paperclip default configurations
{
  hash_data:  ":id/:class/:id/:attachment/:id/:style", #/:updated_at",
  url: "/system/:class/:id_partition/:attachment/:hash.:extension",
  path: ":rails_root/public:url",

  hash_data:  ":id/:class/:id/:attachment/:id/:style", #/:updated_at",
  :path=>"#{Rails.root}/public:url",
  :hash_secret => Rails.configuration.app[:paperclip][:key],
  # :default_style=>:original,
  # :default_url=>"/:attachment/:style/missing.png",
  # :escape_url=>true,
  # :restricted_characters=>/[&$+,\/:;=?@<>\[\]\{\}\|\\\^~%# ]/,
  # :hash_digest=>"SHA1",
  # :interpolator=>Paperclip::Interpolations,
  # :only_process=>[],
  # :preserve_files=>false,
  # :processors=>[:thumbnail],
  # :source_file_options=>{},
  # :storage=>:filesystem,
  # :styles=>{},
  # :url_generator=>Paperclip::UrlGenerator,
  # :use_default_time_zone=>true,
  # :use_timestamp=>true,
  # :whiny=>true,
}.each do |k,v|
  Paperclip::Attachment.default_options[k] = v
end

