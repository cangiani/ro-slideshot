 # encoding: utf-8
#
# background processing:
# https://github.com/lardawge/carrierwave_backgrounder
# background processing backend (suggested by octave)
# http://railscasts.com/episodes/366-sidekiq
# https://github.com/mperham/sidekiq
# alternative is reque (looks like Steve Klabnik works on this):
# https://github.com/resque/resque
# https://github.com/blog/542-introducing-resque
# http://www.mediasoftpro.com/aspnet-x264-presets.html
# https://trac.ffmpeg.org/wiki/EncodeforYouTube
# https://github.com/streamio/streamio-ffmpeg
# https://trac.ffmpeg.org/wiki/x264EncodingGuide
# http://sonnati.wordpress.com/2011/08/19/ffmpeg-–-the-swiss-army-knife-of-internet-streaming-–-part-iii/
# https://github.com/blueimp/jQuery-File-Upload/wiki/Rails-setup-for-V6

class VideoUploader < CarrierWave::Uploader::Base
  # include CarrierWave::Video

  # include ::CarrierWave::Backgrounder::Delay

  # ::CarrierWave::Workers::ProcessAsset.instance_variable_set('@queue', "transcode")
  # ::CarrierWave::Workers::StoreAsset.instance_variable_set('@queue', "transcode")

   # ::CarrierWave::Workers::ProcessAsset.sidekiq_options :queue => "transcode"

  # include ::CarrierWave::Backgrounder::Delay
  # ::CarrierWave::Workers::StoreAsset.instance_variable_set('@queue', "#{Rails.env}_store_asset")

  # avoid double copy by allowing mv
  def move_to_cache
    true
  end

  def move_to_store
    true
  end

  storage :file        # :s3, :fog

  # def extension_white_list
  #   %w(mp4 mpeg4 MTS mts ts avi mov mkv)
  # end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def filename
    @name ||= "#{secure_token}.#{file.extension}" if original_filename
  end

  # process :encode_sticazzi

  # version(:hr_mp4, :if => :processable?) do
  version :mp4 do
    process :process_mp4
    def full_filename(for_file = model.video.file)
      change_extension(super(for_file), "mp4")
    end
  end

  version :ogg do
    process :process_ogg
    def full_filename(for_file = model.video.file)
      change_extension(super(for_file), "ogg")
    end
  end

  version :webm do
    process :process_webm
    def full_filename(for_file = model.video.file)
      change_extension(super(for_file), "webm")
    end
  end

 protected

  def touch
    Rails.logger.debug("touch methods: #{self.methods.sort}")
    Rails.logger.debug("touch         path = #{self.path}")
    Rails.logger.debug("touch current_path = #{self.current_path}")
    Rails.logger.debug("touch store_path   = #{self.store_path}")
    File.open(self.path, "w") {}
    model.video_needs_processing=true
  end

  def process_mp4
    Rails.logger.debug("Process mp4 video")
  end
  def process_ogg
    Rails.logger.debug("Process ogg video")
  end
  def process_webm
    Rails.logger.debug("Process webm video")
  end

 private

  def secure_token
    ivar = "@#{mounted_as}_secure_token"
    token = model.instance_variable_get(ivar)
    token ||= model.instance_variable_set(ivar, SecureRandom.hex(8))
  end

  def change_extension(pn, new_ext)
    old_ext = File.extname(pn)
    base_name = pn.chomp(old_ext)
    base_name << "." << new_ext
  end
end
