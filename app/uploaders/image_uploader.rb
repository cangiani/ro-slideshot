# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base
  # include ::CarrierWave::Backgrounder::Delay
  # include CarrierWave::MimeTypes

  # Include RMagick or ImageScience support
  include CarrierWave::RMagick
  #     include CarrierWave::ImageScience

  storage :file

  # process :set_content_type

  # Override the directory where uploaded files will be stored
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url
    puts "version_name=#{version_name}"
    ActionController::Base.helpers.asset_path([version_name, "placeholder.png"].compact.join('_'))
  end

  version :full do
    process :resize_to_fit => [600,450]
  end

  version :large do
    process :resize_to_fit => [240,240]
  end

  version :med do
    process :resize_to_fit => [128,100]
  end

  version :thmb do
    process :resize_to_fit => [60,45]
  end

end
