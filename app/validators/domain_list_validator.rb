class DomainListValidator < ActiveModel::EachValidator
  # matches two type of domains:
  # 1. an address like www.epfl.ch, www.intranet.epfl.ch, or epfl.ch (www will be added by default)
  # 2. an ip address => 128.178.70.37
  DOMAIN_RE=/^((([a-z][a-z0-9\-_]+\.)+[a-z][a-z0-9\-_]+)|(localhost)|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))$/i
  def validate_each(record, attribute, value)
    domains = value.is_a?(Array) ? value : [value]
    v=true
    domains.compact.each do |d|
      if DOMAIN_RE.match(d).nil?
        v=false
        record.errors.add(attribute, "'#{d}' is not a valid domain")
      end
    end
    v
  end

end
