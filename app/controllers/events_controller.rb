class EventsController < ApplicationController
  before_action :set_event, except: [:index]

  def index
    @events = Event.order(:last_published_at).reverse_order.paginate(:page => params[:page], :per_page => 10)
  end

  def show
  end

  def embed
    @user = @event.user
    if @user.can_embed_from?(request.referer)
      session[:embed] = true
      render :layout => 'embed'
    else
      render :template => "errors/error_noembed.html", :layout => false, :status => :forbidden
    end
    response.headers.except! 'X-Frame-Options'
  end

 private

  def set_event
    sid=params[:id]
    id=sid.to_i
    @event = id==0 ? Event.where("lower(nick) = ?", sid.downcase).last : Event.find(id)
    @talks = @event.talks.listable.order(:id).order(:date)
  end

end
