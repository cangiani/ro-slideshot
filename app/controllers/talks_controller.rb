class TalksController < ApplicationController
  def index
    cat=params[:category]
    @lecture = params.key?(:category) && cat == "lecture"
    @talks = Talk.listable.where(lecture: @lecture).reverse_order.order(:published_at).paginate(:page => params[:page], :per_page => 20)
  end

  def show
    @talk = Talk.find(params[:id])
    @embed = params[:embed]
    authorize! :read, @talk
    respond_to do |format|
      format.js
      format.html
    end
  end
end
