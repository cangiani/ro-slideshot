class ApplicationController < ActionController::Base
  # helper_method :current_user

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def current_user
    nil
  end
end
