class SlideshotsController < ApplicationController
  before_action :set_talk
  respond_to :html

  # GET /slideshots/1
  # GET /play/:nick
  # GET /play/:id
  def show
  end

  def embed
    if session[:embed] or @talk.user.can_embed_from?(request.referer)
      session[:embed] = nil
      render :layout => 'embed'
    else
      render :template => "errors/error_noembed.html", :layout => false, :status => :forbidden
    end
    response.headers.except! 'X-Frame-Options'
  end

  def set_talk
    sid=params[:id]
    if sid =~ /[^0-9]/
      # sid contains at least one non digit char it is interpreted as nick
      @talk=Talk.where("lower(nick) = ?", sid.downcase).first || not_found
      # Rails.logger.debug("can play? #{can? :play, @talk}    playable=#{@talk.playable?}  published=#{@talk.published?}   talk=#{@talk}")
      authorize! :play, @talk
      @talk.plays.create(request: request) unless current_user && @talk.user_id == current_user.id
    else
      # sid contains only digits => it is interpreted as id
      id=sid.to_i
      @talk=Talk.find(id)
      authorize! :play_by_id, @talk
      Play.create(talk: @talk, request: request) unless current_user && @talk.user_id == current_user.id
    end
    @slides = @talk.noslides? || @talk.visible_slides
  end

end
