class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  serialize :embed_from, Array
  validates :embed_from, domain_list: true

  has_many :events
  has_many :talks

  def can_embed_from?(url)
    return false unless self.can_embed?
    return false if self.embed_from.blank?
    not (url =~ embed_from_re).nil?
  end

  def embed_from_re
    @embed_from_re ||= begin
      ef = embed_from
      ef << "localhost"
      ef << Rails.configuration.app[:site][:host]
      d=embed_from.map{|e| e.gsub(".", "\\.")}.map{|e| "(#{e})"}.join("|")
      Regexp.new("^https?://(www\\.)?(#{d})(:[0-9]+)?(/.*|$)")
    end
  end

end
