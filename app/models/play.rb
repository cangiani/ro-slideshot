class Play < ApplicationRecord
  belongs_to :talk, :class_name => "Talk", :foreign_key => "talk_id", :counter_cache => true

  # after_create Settings.geolocation.async ? :complete_geocoding_async : :complete_geocoding
  validate :not_a_bot, :not_from_blacklisted_ip

  before_save :truncate_agent

  geocoded_by :ip_address
  # reverse_geocoded_by :latitude, :longitude
  reverse_geocoded_by :latitude, :longitude do |obj,results|
    if geo = results.first
      obj.address = geo.address
      obj.country = geo.country
    end
  end

  XRE_IPADDR = Regexp.union(
    /128\.178\.70\.170/,            # Giovanni
    /128\.178\.109\.34/,            # EPFL Google Bot
    /155\.105\.4\.141/,
    # /128\.178\.151\.56/,            # stronzi. no stronzo io!
    # /178\.211\.231\.187/,
    # /216\.240\.144\.74/,
    # /128\.179\.154\.108/,
    # /128\.178\.70\.7/,
    # /128.176.171.246/,
    # /173\.252\.103\.7/,
    # /107\.22\.92\.230/,
    # /173\.252\.103\.7/,
    # /23\.21\.236\.154/,
    # /176\.9\.185\.156/,
  )
  XRE_IPADDR_MORE = Regexp.union(
    /128\.178\.109\.34/,            # EPFL Google Bot
    /37\.46\.118\.10/,              #
    /100\.43\.81\./,                # TweetedTimes Bot
    /137\.110\.244\.139/,           # www.integromedb.org/Crawler
    /142\.54\.184\.10/,             # MJ12bot
    /162\.210\.196\.98/,
    /162\.223\.29\.219/,
    /173\.208\.183\.50/,
    /204\.12\.247\.162/,
    /208\.110\.91\./,
    /46\.165\.197\.151/,
    /192\.187\.11[45]\./,
    /173\.192\.79\.101/,            # ShowyouBot
    /144\.76\./,                    # SISTRIX
    /157\.55\.3[23456]\./,          # bingbot
    /157\.56\.229\./,
    /157\.56\.9[23]\./,
    /65\.55\.24\./,
    /65\.55\.52\./,
    /65\.55\.55\./,
    /178\.255\.215\./,              # Exabot
    /180\.46\.149\.233/,            # Robot
    /184\.72\.183\.45/,             # CCBot
    /199\.16\.156\./,               # Twitterbot
    /199\.30\.20\./,                # msnbot-media
    /65\.55\.213\./,                # msnbot
    /199\.59\.148\./,               # Twitterbot
    /208\.115\.111\.72/,            # Ezooms
    /208\.43\.251\.180/,
    /216\.52\.242\.14/,             # LinkedInBot
    /37\.59\.1[68]\./,              # PaperLiBot
    /38\.111\.147\.84/,             # TurnitinBot
    /46\.236\.2[346]\./,            # TweetmemeBot
    /46\.236\.7\./,
    /5\.10\.83\./,                  # AhrefsBot
    /5\.9\.112\.68/,                # SISTRIX
    /54\.196\.163\.76/,             # BufferBot
    /54\.241\.48\.154/,             # PercolateCrawler
    /66\.249\.6[45678]\./,          # Googlebot
    /66\.249\.7[34569]\./,
    /68\.180\.22[45]\./,            # Yahoo! Slurp
    /74\.202\.243\./,               # Socialradarbot
    /76\.73\.3\.18/,                # CompSpyBot
    /77\.56\.237\.113/,             # Kiodia Spider
    /77\.75\.7[37]\./,              # SeznamBot
    /78\.46\.34\.151/,              # uMBot-LN
    /89\.145\.95\.42/,              # GrapeshotCrawler
    /95\.108\.154\.252/,            # YandexBot
    /213\.167\.96\.68/,             # Twitter ?
    /94\.126\.16\.15/,                 # wiederfreibot
    /81\.144\.138\.34/,                # Wotbox
    # /198\.27\.70\.212/,
    # /5\.9\.125\.26/,
    # /193\.138\.213\.203/,
    # /176\.9\.21\.194/,
    # /66\.249\.78\.84/,
    # /77\.248\.44\.111/,
    # /5\.9\.223\.170/,
    # /66\.249\.78\.84/,
    # /62\.194\.176\.95/,
    # /162\.243\.121\.214/,
    # /198\.100\.144\.55/,
    # /5\.135\.46\.155/,
    # /88\.198\.40\.163/,
    # /66\.249\.78\.84/,
    # /107\.23\.94\.206/,
  )

  XRE_AGENTS = Regexp.union(
    /Robot/, /Bot/, /Crawler/, /web spider/, /Spider/, /Googlebot/, /gsa-crawler/,
    /msnbot/, /Yahoo! Slurp/, /PagesInventory/, /Superfeedr/, /Yandex/,
    /spbot/, /SiteExplorer/, /proximic/, /NetcraftSurveyAgent/, /MJ12bot/,
    /Socialradarbot/, /Ezooms/, /Exabot/, /bingbot/, /Twitterbot/, /bingbot/,
    /Baiduspider/, /cis455crawler/, /Java/, /Wget/, /newsme/, /Slurp/, /Ruby/,
    /Wotbox/, /wiederfreibot/, /psbot/, /^-$/, /Jakarta/, /BUbiNG/, /Python-urllib/
  )
  # XRE_AGENTS = Regexp.union(
  #   /[Bb]ot[^a-zA-Z]/, /[Cc]rawler/, /[Ss]pider/, /Spider/,
  #   /Java/, /Wget/, /newsme/, /Slurp/, /Ruby/, /^-$/
  #   /newsme/, /Ezooms/, /NetcraftSurveyAgent/, /proximic/, /SiteExplorer/,
  #   /Yandex/, /Yahoo! Slurp/, /PagesInventory/, /Superfeedr/,
  # )

  def request=(r)
    self.ip_address = r.remote_ip
    self.agent = r.user_agent
  end

  def request
    nil
  end

  # def self.create_unless_crap(talk, request, p={})
  #   # ag=request.env['HTTP_USER_AGENT']
  #   ag=request.user_agent
  #   ip=request.remote_ip
  #   puts "ip=#{ip} ag=#{ag}"
  #   return nil if ip =~ XRE_IPADDR
  #   return nil if ag =~ XRE_AGENTS
  #   talk.plays.create({ip_address: ip, agent: ag}.merge(p))
  # end

  def complete_geocoding(max_attempts=0)
    if geocode_attempts <= max_attempts
      unless geocode && reverse_geocode
        geocode_attempts += 1
      end
      save
    end
  end


 private

  def not_a_bot
    m = self.agent.present? ? XRE_AGENTS.match(self.agent) : XRE_IPADDR_MORE.match(self.ip_address)
    unless m.nil?
      errors.add(:agent, "Agent is listed among bots/crawlers")
    end
  end

  def not_from_blacklisted_ip
    unless XRE_IPADDR.match(ip_address).nil?
      errors.add(:ip_address, "Ip address is blacklisted")
    end
  end

  def complete_geocoding_async
    if Settings.geolocation.async
      CompletePlayGeocodingWorker.perform_async(id)
    else
      complete_geocoding
    end
  end

  def truncate_agent
    self.agent = self.agent[0..254] if (self.agent.present? && self.agent.length > 255)
  end
end
