class Slide < ApplicationRecord
  require 'carrierwave/orm/activerecord'

  mount_uploader :image, ImageUploader
  # process_in_background :image
  # store_in_background :image

  belongs_to :talk

  scope :ready, -> { where(image_processing: nil)}
  scope :processing, -> { where(image_processing: true)}
  scope :bytime, -> { order(:show_at) }
  scope :trashed, -> { where(trashed: true) }
  scope :untrashed, -> { where(trashed: false) }

  def at
    [(self.show_at||0) + self.talk.offset, 0].max
  end

  def at=(v)
    iv=v.to_i
    self.show_at = iv - self.talk.offset
  end

  def prev_slide_by_name
    @prev_slide_by_name ||= Slide.untrashed.order(:image).where("talk_id = ? AND image < ?", self.talk_id, self.image_name).last
  end

  def prev_slide_by_time
    @prev_slide_by_time ||= Slide.untrashed.bytime.where("talk_id = ? AND show_at < ?", self.talk_id, self.show_at).last
  end

  def next_slide_by_name
    @next_slide_by_name ||= Slide.untrashed.order(:image).where("talk_id = ? AND image > ?", self.talk_id, self.image_name).first
  end

  def next_slide_by_time
    @next_slide_by_time ||= Slide.untrashed.bytime.where("talk_id = ? AND show_at > ?", self.talk_id, self.show_at).first
  end

  def prev_slide(by_name=false)
    @prev_slide ||= self.show_at.present? && !by_name ? prev_slide_by_time : prev_slide_by_name
  end

  def next_slide(by_name=false)
    @next_slide ||= self.show_at.present? && !by_name  ? next_slide_by_time : next_slide_by_name
  end

  def prev_at(by_name=false)
    prev_slide && prev_slide.show_at.present? ? prev_slide.at : 0
  end

  def next_at(by_name=false)
    next_slide && next_slide.show_at.present? ? next_slide.at : -1
  end

  def image_name
    attributes["image"]
  end

  # TODO: this is a dirty fix for a strange error in carrierwave. Might break everything!!!!!
  def image_changed?
    false
  end

  def slideshot_dir=(path)
    Rails.logger.debug "Slide.dir=#{path}"
    meta_path=path+"/meta.d"
    new_meta_path=path+"/meta.yml"
    image_path1=path+"/fullres.png"
    image_path2=path+"/fullres.jpg"
    @poll = nil
    d = 0
    if File.exists?(path) && File.directory?(path) && (File.exists?(meta_path) || File.exists?(new_meta_path)) && File.exists?(image_path1) || File.exists?(image_path2)
      if (File.exists?(new_meta_path))
        meta=YAML.load_file(new_meta_path)
        d=meta['shotOffset']
        # @poll = meta["poll"]
      else
        d=`grep "shotOffset" #{meta_path}`.split[1].to_i * 1000
      end
      image_path = File.exists?(image_path1) ? image_path1 : image_path2
      self.show_at=d
      # rename the file so in case of db corruption we still have a way to recover the original delays
      dpath=File.dirname(image_path)+"/"+sprintf("%09d", d)+File.extname(image_path)
      Rails.logger.debug "--------- image_path=#{image_path}  =>   dpath=#{dpath}"
      File.rename(image_path, dpath)
      self.image=File.new(dpath)
    end
  end

  # before_create :eventually_set_show_at

  # def dir=(path)
  #   meta_path=path+"/meta.yml"
  #   image_path=path+"/fullres.jpg"
  #   if File.exists?(path) && File.directory?(path) && File.exists?(meta_path) && File.exists?(image_path)
  #     d=`grep "shotOffset" #{meta_path}`.split[1]
  #     self.show_at=d
  #     # rename the file so in case of db corruption we still have a way to recover the original show_ats
  #     dpath=File.dirname(image_path)+"/"+sprintf("%05d.jpg", d)
  #     File.rename(image_path, dpath)
  #     self.image=File.new(dpath)
  #   end
  # end

  # def eventually_set_show_at
  #   unless self.show_at
  #     f=self.image.path
  #     dstring=File.basename(f, File.extname(f)).match('[0-9]*')[0]
  #     unless (dstring.empty?)
  #       self.show_at = dstring.to_i
  #       Rails.logger.debug("Slide for file #{f}. Setting show_at to #{self.show_at} s")
  #     end
  #   end
  # end

end
