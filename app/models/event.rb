class Event < ApplicationRecord
  belongs_to :user
  has_many :talks

  validates_presence_of :user_id, :on => :create, :message => "can't be blank"
  validates_presence_of :nick, :on => :create, :message => "can't be blank"
  validates_length_of   :nick, :within => 1..16, :on => :create, :message => "Should be at most 16 characters long"
  validates_presence_of :title, :on => :create, :message => "can't be blank"

  mount_uploader :image, ImageUploader

  def nickname
    nick
  end
end
