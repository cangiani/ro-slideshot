class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
    user ||= User.new

    # guest user
    can :play, Talk do |t|
      t.playable? && (t.published? || t.user_id == user.id)
    end

    can :read, Talk do |t|
      t.listable?
    end

    can :play_by_id, Talk do |t|
      t.playable? && (t.listable? || t.user_id == user.id)
    end

    # if user.has_role? :admin
    #   can :manage, :all
    # else
      can :create, Event
      can :create, Talk
      can :create, Slide
      can [:read, :update], Talk do |t|
        t.user_id == user.id
      end
      can [:read, :update], Event do |e|
        e.user_id == user.id
      end
      can [:read, :update, :destroy], Slide do |s|
        s.talk.user_id == user.id
      end
    # end
  end
end
