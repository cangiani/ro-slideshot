class Talk < ApplicationRecord

  has_many :slides
  # belongs_to :user
  belongs_to :event
  belongs_to :location

  has_many   :plays

  mount_uploader :image, ImageUploader

  include VideoclipLocal
  include VideoclipYoutubeAlien
  include VideoclipLegacy

  scope :listable, -> { where(published: true, private: false) }
  scope :featured, -> { listable.where.not(featured_at: nil) }
  scope :last_published, ->(count=5) { listable.order(:published_at).reverse_order.limit(count) }
  scope :last_featured, ->(count=5) { featured.order(:featured_at).reverse_order.limit(count) }

  def self.find_by_sid(sid)
    if sid =~ /[^0-9]/
      talk=Talk.where("lower(nick) = ?", sid.downcase).first
      raise ActiveRecord::RecordNotFound, "Couldn't find Talk with sid='#{sid}'" if talk.nil?
      return talk
    else
      id=sid.to_i
      talk=Talk.find(id)
      return talk
    end
  end

  def to_jq_upload
    {
      "name" => read_attribute(:video),
      # "size" => video.size,
      # "url" => avatar.url,
      # "thumbnail_url" => avatar.thumb.url,
      # "delete_url" => picture_path(:id => id),
      # "delete_type" => "DELETE"
    }
  end


  # TODO: add validation on uniqueness of nick (donwcase)

  def listable?
    self.published && !self.private
  end

  def playable?
    video_playable? && (slides.count>0 || noslides?)
  end

  def publishable?
    !published && video_playable? && ( slides.count>0 && offset.present? || noslides? )
  end

  def featured?
    !self.featured_at.nil?
  end

  def featurable?
    self.listable?
  end

  def featured
    self.featured?
  end

  def featured!
    self.update_attribute(:featured_at, Time.now) if self.featurable?
  end

  def unfeatured!
    self.update_attribute(:featured_at, nil)
  end

  def featured=(v)
    if featured?
      self.update_attribute(:featured_at, nil) unless v
    else
      featured! if v
    end
  end

  def toggle_featured
    if self.featured?
      self.update_attribute(:featured_at, nil)
    else
      featured!
    end
  end

  def publish!
    if publishable?
      self.published = true
      self.published_at = Time.now
      if save
        if e=self.event
          e.last_published_at = self.published_at
          e.save
        else
          true
        end
      else
        false
      end
    else
      false
    end
  end

  def syncable?
    video_playable? && slides.count>0
  end

  def nick_or_id
    self.nick && !self.nick.empty? ? self.nick : self.id
  end

  def event_nick
    self.event.nil? ? "" : self.event.nick
  end

  # remove all the slides that happen before the first one (if any)
  def visible_slides
    ss=self.slides.untrashed.bytime
    while(ss.length>1 && ss[1].at==0)
      # puts "#{ss[0].at} - #{ss[1].at}"
      ss.shift
    end
    ss
  end

  def eventually_set_nick_for_private_video
    if self.private? && self.nick.blank?
      n="k"+SecureRandom.urlsafe_base64(8)
      while Talk.where(:nick => n).count > 0
        n="k"+SecureRandom.urlsafe_base64(8)
      end
      self.nick = n
    end
  end
  # -------------------------------------------------------- For synchronization
  def sync_slide_id
    @sync_slide ? @sync_slide.id : nil
  end

  def sync_slide_id=(i)
    @sync_slide = Slide.find(i)
  end

  def sync_time
    @sync_time
  end

  def sync_time=(t)
    @sync_time=t.to_i     # already in ms
  end

  def compute_offset
    unless @sync_time.nil? || @sync_slide.nil?
      puts "sync: offset prima = #{offset}"
      self.offset=sync_time - @sync_slide.show_at
      puts "sync: offset dopo  = #{offset}"
    end
  end

  # -------------------------------------------------------------- YouTube Video

  def youtube_title
    ml=60
    i=self.id.to_s
    e=self.event
    s=self.speaker
    t=self.title
    if e
      e = e.nick
      tb=i + " - " + e + " - " + s + ": "
      tt=tb + t
      if tt.length > ml
        if tb.length < ml - 3
          dl = ml - tb.length
          tt = tb + t.truncate(dl)
        else
          tb = i + " - " + e + " - "
          if tb.length < ml - 3
            dl = ml - tb.length
            tt = tb + s.truncate(dl)
          else
            tb = i + " - "
            dl = ml - tb.length
            tt = tb + t.truncate(dl)
          end
        end
      end
    else
      tb=i + " - " + s + ": "
      tt=tb + t
      if tt.length > ml
        if tb.length < ml - 3
          dl = ml - tb.length
          tt = tb + t.truncate(dl)
        else
          tb = i + " - "
          dl = ml - tb.length
          tt = tb + t.truncate(dl)
        end
      end
    end
    tt
  end

  def youtube_video_metadata
    {
      :title => youtube_title,
      :description => abstract,
      :category=>"Education",
      :private => false,
      :rate => "denied",
      :comment => "denied",
      :commentVote => "denied",
      :list => "denied",
      :embed => "allowed",
      :syndicate => "denied"
    }
  end

end
