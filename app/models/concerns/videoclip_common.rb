# This is the "abstract" base Videoclip module.
# It requires the model to have the following migration added:
#  t.string :preferred_player, default: "local"

module VideoclipCommon
  extend ActiveSupport::Concern
  included do
    self.videoclip_modules=[]

    def video_players
      @video_players ||= begin
        r=[]
        self.class.videoclip_modules.each do |m|
          r << m if eval "#{m}_video_ok?"
        end
        r
      end
    end

    # This one might be overridden
    def video_playable?
      !video_players.empty?
    end

    def video_player(prefer=preferred_player)
      return nil unless video_playable?
      if video_players.include?(prefer)
        prefer
      elsif video_players.include?(preferred_player)
        preferred_player
      else
        video_players.first
      end
    end
  end

  module ClassMethods
    attr_accessor :videoclip_modules
  end
end
