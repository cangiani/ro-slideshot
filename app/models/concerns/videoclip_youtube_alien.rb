# This is the Videoclip module for serving videoclips that are already on YouTube.
# It requirest the following migrations in the model:
# t.string     :youtube_vid
# t.string     :youtube_video_state, default: "missing"
module VideoclipYoutubeAlien
  extend ActiveSupport::Concern

  include VideoclipCommon

  included do
    @videoclip_modules << "youtube"

    def youtube_url
      youtube_vid.present? ? "https://www.youtube.com/watch?v=#{youtube_vid}" : nil
    end

    def youtube_video_ok?
      respond_to?(:youtube_vid) && youtube_vid.present? && youtube_video_state == "ready"
    end

    # --------------------------------------------------------------------------
  end
end
