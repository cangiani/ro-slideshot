# This is the Videoclip module for serving videoclips from YouTube with the
# capability of uploading local video files to YouTube
# It requirest the following migrations in the model:
# t.string  :youtube_vid
# t.string  :youtube_video_state, default: "missing"
# t.integer :youtube_upload_attempts, default: 0
#
# The class using this concern have to define a method :youtube_video_metadata
# returning an hash like the follosing:
# {
#   :title => "This is the title that the video will have on YouTube",
#   :description => "Lorem ipsum dolor sit amet, consectetur adipisicing...",
#   :category=>"Education",
#   :private => false,
#   :rate => "denied",
#   :comment => "denied",
#   :commentVote => "denied",
#   :list => "denied",
#   :embed => "allowed",
#   :syndicate => "denied"
# }
#
# Uploading to YouTube is taken care by the YouTubeIt gem. The credentials for
# connecting to youtube are returned by the youtube_credentials method that can
# be overriden in the class definition after including this module. The default
# uses settingslogic gem and expects the following definitions to be present:
# video:
#   youtube:
#     credentials:
#       dev_key: 'your_dev_key_here'
#       username: 'your youtube username@gmail.com'
#       password: 'your password'

require 'youtube_it'

module VideoclipYoutube
  extend ActiveSupport::Concern

  include VideoclipLocal
  include VideoclipYoutubeAlien

  included do

    after_commit :eventually_process_youtube_video


   private

    def eventually_process_youtube_video
      if local_video? && preferred_player == "youtube" && youtube_video_state == "missing"
        write_attribute(:youtube_video_state, "processing")
        save
        # TODO: set youtube_video_state to "processing" and upload video to youtube

        # WARNING: there might be concurrency problems!!
      end
    end

    def youtube_video_metadata
      raise "Please define a youtube_video_metadata method in #{self.class}."
    end

    def youtube_credentials
      Rails.configuration.app[:video][:youtube][:credentials]
    end

    def upload_to_youtube!(max_failed=1, force=false)
      logprefix="#{self.class} VideoclipYoutube::upload_to_youtube:"
      unless self.local_video.present?
        Rails.logger.debug "#{logprefix} skipping because original video is not present."
        return false
      end
      unless youtube_vid.blank? || force
        Rails.logger.debug "#{logprefix} skipping because there is already a youtube video id: #{youtube_vid}"
        return false
      end
      if force
        Rails.logger.debug "#{logprefix} uploading video to youtube even if a youtube video id is present"
      end
      if youtube_upload_attempts > max_failed
        Rails.logger.debug "#{logprefix} skipping for exceded max number of attempts."
        return false
      end
      filename = local_video_path(:original)
      video_data = youtube_video_metadata
      Rails.logger.debug "#{logprefix} Trying to upload video for #{self.class} with id=#{self.id}"
      begin
        self.update_attribute(:youtube_upload_attempts, self.youtube_upload_attempts+1)
        client = YouTubeIt::Client.new(youtube_credentials)
        r=client.video_upload(File.open(filename), video_data)
      rescue Exception => e
        Rails.logger.error "#{logprefix} Error uploading video for #{self.class} with id #{self.id} to youtube."
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.inspect
        return false
      end
      vid=r.video_id.gsub(/^.*:video:/, "")
      Rails.logger.info "#{logprefix} Upload for talk #{self.id} completed. YouTube id=#{vid}"
      self.update_attributes(youtube_id: vid, youtube_upload_attempts: 0)
      return true
    end

  end
end
