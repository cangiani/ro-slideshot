module VideoclipLegacy
  extend ActiveSupport::Concern

  include VideoclipCommon

  included do
    @videoclip_modules << "legacy"

    has_attached_file(
      :video,
      :styles => { :mp4 => {format: "mp4"}, :ogg => {format: "ogg"}, :webm => {format: "webm"} },
      # :styles => { :mp4 => {format: "mp4"} },
      :processors => [:video_processor],
      :hash_data => ":class/:attachment/:id/:style/",
      :url=>"/uploads/:class/:attachment/:id_partition/:hash.:extension",
    )
    has_attached_file(
      :original_video,
      hash_data: ":class/:attachment/:id/:style/",
      :url=>"/uploads/:class/:attachment/:id_partition/:hash.:extension",
    )

    # after_original_video_post_process :on_original_video_processing_done
    # after_video_post_process :on_video_processing_done

    # after_commit :eventually_process_video

    def legacy_video_state
      video_status || "missing"
    end

    def legacy_video_ok?
      video? && video_status == "ready"
    end

    # # this can be just a sidekiq job that assign to video the file in original_video
    # # or, as in my case, just set video_processing to true so that an external program
    # # knows which talks have video to be transcoded
    # def on_original_video_processing_done
    #   Rails.logger.debug("            on_original_video_processing_done")
    #   # puts "---------- on_original_video_processing_done: #{original_video.path}   #{File.exists? original_video.path}"
    #   self.video_status = "uploaded" if (self.video_status==nil || self.video_status == "error")
    #   @needs_processing = true
    #   true
    # end

    # def eventually_process_video
    #   Rails.logger.debug("                       needs_processing: #{@needs_processing}    status=#{self.video_status}")
    #   # puts "---------- needs_processing: #{@needs_processing}    status=#{self.video_status}"
    #   if @needs_processing && video_status == "uploaded"
    #     # puts "---------- eventually_process_video: #{original_video.path}   #{File.exists? original_video.path}"
    #     if Rails.configuration.app[:video][:legacy][:async]
    #       ProcessVideoWorker.perform_async(id) # this will call the process_video! method below
    #     else
    #       process_video!
    #     end
    #   end
    # end

    # def process_video!
    #   if video_status == "uploaded"
    #     if youtube_enabled?
    #       upload_to_youtube!
    #     else
    #       if Rails.configuration.app[:video][:legacy][:qsub]
    #         QsubVideoProcessWorker.perform_async(id)
    #       else
    #         transcode!(:mt => true)
    #       end
    #     end
    #   end
    # end

    # # TODO: return true only when it works!
    # def transcode_with_copy!
    #   return false unless original_video.present? && video_status == "uploaded"  # && !youtube_enabled?
    #   update_attribute(:video_status, "processing")
    #   self.video = File.open(original_video.path)
    #   if self.save && File.unlink(self.video.path) && FileUtils.ln_s(self.original_video.path, self.video.path)
    #     true
    #   else
    #     false
    #   end
    # end

    # # this takes the video from original video and transcode it into the various versions for video
    # # avoiding to do many useless file copies faking the way paperclip works. Th
    # # if all files are local (e.g. not on s3).
    # def transcode!(opts={})
    #   return false unless original_video.present? && video_status == "uploaded" #  && !youtube_enabled?
    #   tc=VideoTranscoder.new(self.original_video.path)
    #   return false unless tc.valid?

    #   mt = opts[:mt] || false

    #   Rails.logger.debug "Transcoding video for talk #{self.id}"
    #   self.video_file_name = self.original_video_file_name
    #   self.video_content_type = self.original_video_content_type
    #   self.video_file_size = self.original_video_file_size
    #   self.video_status = "processing"
    #   return false unless self.save
    #   d=File.dirname(self.video.path)
    #   unless FileUtils.mkdir_p(d)
    #     self.update_attribute(:video_status, "error")
    #     Rails.logger.error "Could not create video directory #{d} for talk #{self.id}"
    #     false
    #   else
    #     FileUtils.ln_s(self.original_video.path, self.video.path) unless File.exists?(self.video.path)
    #     begin
    #       # parallel version: do ogg on a separate thread as it takes much longer than the other two
    #       # because libtheora is single threaded while webm and mp4 use ~ 4 cores.
    #       if mt
    #         t1=Thread.new {
    #           tc.transcode(self.video.path(:ogg), :ogg)
    #         }
    #         t1.abort_on_exception = true
    #         tc.transcode(self.video.path(:mp4), :mp4)
    #         tc.transcode(self.video.path(:webm), :webm)
    #         t1.join
    #       else
    #         tc.transcode(self.video.path(:mp4), :mp4)
    #         tc.transcode(self.video.path(:ogg), :ogg)
    #         tc.transcode(self.video.path(:webm), :webm)
    #       end
    #       self.update_attributes(:video_updated_at => Time.now, :video_status => "ready")
    #     rescue Exception => e
    #       Rails.logger.error("Exception in video transcoding: #{e.message}")
    #       self.update_attributes(:video_status => "error")
    #       false
    #     end
    #   end
    #   true
    # end


  end
end
