# This is the Videoclip module for stiring and transcoding videoclips on the
# local filesystem.
# WARNING: this is just an hack that breaks the nice generic way of working of
# paperclip and is only valid for LOCAL FILES.
# It requirest the following migrations in the model:
# t.attachment :local_video
# t.boolean    :local_video_processing, default: false
# t.string     :local_video_state, default: "missing"

# It requires the model to have the following migration added:
#  t.string :preferred_player, default: "local"
module VideoclipLocal
  extend ActiveSupport::Concern

  include VideoclipCommon

  included do

    @videoclip_modules << "local"

    # --------------------------------------------------------------------------

    has_attached_file(
      :local_video,
      styles: { :hls => {format: "m3u8"}, :mp4 => {format: "mp4"}, :webm => {format: "webm"} },
      preserve_files: "true",
    )

    begin
      # post processing is done only if validations are passed
      validates_attachment_content_type :local_video, content_type:  /\A(application\/(mp4|octet-stream)|video\/.*)\z/
      validates_attachment_file_name :local_video, :matches => [/mts\z/, /mpeg\z/, /mpg\z/, /m4v\z/, /mp4\z/]
    rescue
      Rails.logger.debug "Please do you best to upgrade paperclip gem to ~> 4.3"
    end

    before_local_video_post_process Proc.new {
      # skip standard paperclip post-processing
      false
    }

    after_local_video_post_process Proc.new {
      write_attribute(:local_video_state, "stored")
      true
    }

    # if Rails.configuration.app[:video][:local][:async]
    #   process_in_background :local_video
    # end

    # after_commit :post_process_local_video, if: Proc.new { |v| v.local_video_state == "stored" }

    def local_video=(file)
      write_attribute(:local_video_state, "storing")
      local_video.assign(file)
    end

    def local_video_reprocess!
      post_process_local_video(true)
    end

    def local_video_ok?
      local_video? && local_video_state == "ready"
    end

   private


    def post_process_local_video(force=false)
      return unless self.local_video_state == "stored" || force
      write_attribute(:local_video_state, "processing")
      save
      begin
        transcoder = VideoTranscoder.new(local_video.path(:original))
        local_video.styles.each do |name, obj|
          format = obj.format
          dst = local_video.path(name)
          transcoder.transcode(dst, format)
        end
        write_attribute :local_video_state, "ready"
        save
      rescue
        write_attribute :local_video_state, "error"
        save
      end
    end

  end

end # VideoclipLocal module

# if mt
#   t1=Thread.new {
#     tc.transcode(self.video.path(:ogg), :ogg)
#   }
#   t1.abort_on_exception = true
#   tc.transcode(self.video.path(:mp4), :mp4)
#   tc.transcode(self.video.path(:webm), :webm)
#   t1.join
# else
#   tc.transcode(self.video.path(:mp4), :mp4)
#   tc.transcode(self.video.path(:ogg), :ogg)
#   tc.transcode(self.video.path(:webm), :webm)
# end
