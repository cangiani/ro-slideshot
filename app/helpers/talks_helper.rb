module TalksHelper
  # def render_markdown(t)
  #   BlueCloth.new(t).to_html.html_safe
  # end

  def large_image_for_talk(talk)
    if talk.image.present?
      image_tag talk.image.large.url, class: "img-responsive"
    elsif s=talk.slides.bytime.first
      image_tag s.image.large.url
    elsif talk.event && talk.event.image.present?
      image_tag talk.event.image.large.url, class: "img-responsive"
    else
      image_tag "large_placeholder.png", class: "img-responsive"
    end
  end
  def med_image_for_talk(talk)
    if talk.image.present?
      image_tag talk.image.med.url, class: "img-responsive"
    elsif s=talk.slides.bytime.first
      image_tag s.image.med.url
    elsif talk.event && talk.event.image.present?
      image_tag talk.event.image.med.url, class: "img-responsive"
    else
      image_tag "med_placeholder.png", class: "img-responsive"
    end
  end

  def video_path(t,ext="mp4")
    if t.video.present?
      if t.video_status != "ready"
        asset_path("processing.#{ext}")
      else
        t.video(ext)
      end
    else
      asset_path("default.#{ext}")
    end
  end
  def video_status_label(t)
    l = case t
        when "ready"
          "success"
        when "stored", "storing", "processing"
          "warning"
        when "missing", "absent", "error"
          "danger"
        end
    "<span class='label label-#{l}'>#{h t}</span>".html_safe
  end


  # TODO
  def video_url(t,ext="mp4")
    # "http://192.168.70.38:3000/uploads/talks/videos/000/000/290/0619061dad54675be1906f0b0c5c6a65722f149d.ogg?1439558240".html_safe
    "#{root_url}/#{video_path(t,ext)}".gsub("%2F", "/").gsub("%3F", "?").html_safe
  end
end
