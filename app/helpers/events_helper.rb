module EventsHelper

  def large_image_for_event(event)
    if event.image.present?
      image_tag event.image.large.url, class: "img-responsive"
    else
      image_tag "large_placeholder.png", class: "img-responsive"
    end
  end
  def med_image_for_event(event)
    if event.image.present?
      image_tag event.image.med.url, class: "img-responsive"
    else
      image_tag "med_placeholder.png", class: "img-responsive"
    end
  end

end
