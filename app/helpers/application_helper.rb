module ApplicationHelper

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end

  def header_title t
    content_for :title, t
  end

  def content_title t, st=nil
    content_for :title, t unless content_for? :title
    if st.nil?
      "<h1>#{h(t)}</h1>".html_safe
    else
      "<h1>#{h(t)}<small>%{h(st)}</small></h1>".html_safe
    end
  end

  def tronca(s, l)
    s.present? && s.is_a?(String) ? s.truncate(l) : s
  end

  def yesno_label(b)
    (b ? "<span class='label label-success'>Yes</span>" : "<span class='label label-danger'>No</span>").html_safe
  end

  def noyes_label(b)
    (b ? "<span class='label label-danger'>Yes</span>" : "<span class='label label-success'>No</span>").html_safe
  end

  def with_label(t, l="success")
    "<span class='label label-#{l}'>#{h t}</span>".html_safe
  end

  def icon_with_text(i,t)
    "<i class='bi-#{i}'></i>&nbsp;#{t}".html_safe
  end

  def icon(i)
    "<i class='bi-#{i}'></i>".html_safe
  end

  def edit_with_icon(t="Edit")
    icon_with_text("edit", t)
  end

  def show_with_icon(t="More...")
    icon_with_text("zoom-in", t)
  end

  def index_with_icon(t="List")
    icon_with_text("list", t)
  end

  def play_with_icon(t="Watch")
    icon_with_text("play", t)
  end

  def add_with_icon(t="Add")
    icon_with_text("plus", t)
  end

  def button_to_play(talk, t="Watch")
    link_to play_with_icon(t), play_slideshot_path(talk.nick_or_id), class: "btn btn-primary"
  end

  def button_to_play_for_embed(talk, t="Watch")
    link_to play_with_icon(t), play_embedded_slideshot_path(talk.nick_or_id), class: "btn btn-default btn-xs", data: {'no-turbolink' => true}
  end

  def format_agent(s)
    a = UserAgent.parse(s)
    [a.browser, a.version, a.platform].map{|v| v.nil? ? "unknown" : v}.join(" / ")
  end

  def render_markdown(t)
    # https://github.com/vmg/redcarpet
    @rc_renderer ||= Redcarpet::Render::HTML.new(filter_html: false, hard_wrap: true)
    @rc_markdown ||= Redcarpet::Markdown.new(@rc_renderer, autolink: true, tables: true, space_after_headers: true, highlight: true, no_intra_emphasis: true)
    @rc_markdown.render(t.gsub("\r\n", "\n")).html_safe
  end

  def markdown_example
    @md_example ||= File.read(Rails.root.join('config', 'example.md'))
  end

end
